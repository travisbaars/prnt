# Building PrintPal

## Environment setup

### Clone repository

```bash
git git@gitlab.com:travisbaars/printpal.git
```

### Edit the `.env.example` file and save as `.env`

- Change `PRINTER_IP` to you printer's ip address

- Change `PRINTER_PORT` to your printer's port number

#### Example .env entries

```bash
PRINTER_IP = 192.168.1.25
PRINTER_PORT = 9100
```

## Static Analysis with `pylint`

### Run the following command

```bash
pylint src/printpal/printpal.py --extension-pkg-
allow-list=wx
```

`--extension-pkg-allow-list=wx` prevents certain wx related linting errors.

## Packaging with `pyinstaller`

```bash
pyinstaller --onefile --collect-datas=escpos --collect-datas=wxpython --add-data '.env:.' src/printpal/printpal.py
```
