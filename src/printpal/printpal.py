"""Import graphic and network printer modules"""
import os
import wx

from dotenv import load_dotenv
from escpos.printer import Network


# Simple .env setup
load_dotenv()

# Set constants from .env
PRINTER_IP = os.environ.get("PRINTER_IP")
PRINTER_PORT = int(os.environ.get("PRINTER_PORT"))


# Print action function
def printer_send(
    event: None, data: str, alignment: str, autocut: bool
):  # pylint: disable=W0613
    """Connect to printer instance and send text to print"""

    # Setup network connection
    printer = Network(PRINTER_IP, PRINTER_PORT)

    # Set printing settings
    printer.set(align=alignment, font="b")

    # Text to print
    printer.textln(data)
    #   device.ln(count=13)

    if autocut:
        printer.cut()

    printer.close()


# GUI Main Panel
class MainPanel(wx.Panel):  # pylint: disable=R0903
    """GUI Main panel class"""

    def __init__(self, parent):
        super().__init__(parent)

        # Create boxes
        head_sizer = wx.BoxSizer(wx.HORIZONTAL)
        main_sizer = wx.BoxSizer(wx.VERTICAL)
        sub_sizer = wx.BoxSizer(wx.HORIZONTAL)

        # Justification radio selection labels
        justification_labels = ["left", "center", "right"]

        # Create and bind justification box
        justification_box = wx.RadioBox(
            self, label="Justification", choices=justification_labels
        )
        justification_box.Bind(wx.EVT_RADIOBOX, self.justification_radio_sel)

        # Create cut selector
        cut_selection = wx.CheckBox(self, label="Auto-Cut")

        # Create text box
        textbox = wx.TextCtrl(self, size=(280, 250), style=wx.TE_MULTILINE)

        # Create print and clear buttons
        btn_print = wx.Button(self, label="Print")
        btn_clear = wx.Button(self, label="Clear")

        # Bind print and clear buttons
        btn_print.Bind(
            wx.EVT_BUTTON,
            lambda event: printer_send(
                event,
                textbox.GetValue(),
                justification_box.GetStringSelection(),
                cut_selection.GetValue(),
            ),
        )
        btn_clear.Bind(wx.EVT_BUTTON, lambda event: textbox.SetValue(""))

        # Add items to head sizer
        head_sizer.Add(justification_box, 0, wx.ALL | wx.LEFT, 5)
        head_sizer.Add(
            cut_selection, 0, wx.ALL | wx.RIGHT | wx.ALIGN_CENTER_VERTICAL, 5
        )

        # Add items to main sizer
        main_sizer.Add(head_sizer, 0, wx.ALL | wx.CENTER, 2)
        main_sizer.Add(textbox, 0, wx.ALL | wx.CENTER, 5)

        # Add items to sub sizer
        sub_sizer.Add(btn_print, 0, wx.ALL | wx.CENTER, 2)
        sub_sizer.Add(btn_clear, 0, wx.ALL | wx.CENTER, 2)

        # Add sub sizer to main sizer
        main_sizer.Add(sub_sizer, 0, wx.ALL | wx.CENTER, 2)

        # Set main sizer
        self.SetSizer(main_sizer)

    # Radio box action (justification)
    def justification_radio_sel(self, event):
        """Click action for Justification radio box"""

        # Get associated event object
        event_object = event.GetEventObject()

        # Extract selection string
        selection = event_object.GetStringSelection()

        # Return selection
        return selection


# GUI Main Window
class MainFrame(wx.Frame):  # pylint: disable=R0901, R0903
    """GUI Main Frame Class"""

    def __init__(self):
        # Window setup
        wx.Frame.__init__(
            self, parent=None, title="Convenience Printer", size=(300, 375)
        )
        self.panel = MainPanel(self)

        # Display window
        self.Show()


if __name__ == "__main__":
    app = wx.App(False)
    frame = MainFrame()

    # GUI main loop
    app.MainLoop()
